package pdx2sqlite;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;


public class Main {
    public static void main(String[] argv) throws ParseException, IOException, FileNotFoundException, SQLException, ClassNotFoundException {
        
     Db db = new Db();

     db.dbFileDel();
     db.createSQLiteTables();
     db.cloneParadoxAddressDataToList();
     db.cloneAddressDataToSQLiteObjects();
     //db.cloneParadoxDataToList();
    // db.cloneDataListToSQLiteObjects();
    }
}